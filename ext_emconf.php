<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'Oauth2 manager',
	'description' => 'Extension for managing oauth2 connections',
	'category' => 'be',
	'author' => 'Kay Wienöbst',
	'author_email' => 'kay@hauptsache.net',
	'state' => 'stable',
	'uploadfolder' => false,
	'clearCacheOnLoad' => true,
	'author_company' => 'hauptsache.net GmbH',
	'version' => '1.0.0',
	'constraints' => 
	[
		'depends' => 
		[
			'typo3' => '7.6.0-8.9.99',
        ],
		'conflicts' => 
		[
        ],
		'suggests' => 
		[
        ],
    ],
];

?>