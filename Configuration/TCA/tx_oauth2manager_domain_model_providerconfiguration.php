<?php

return [
    'ctrl' => [
        'title' => 'Provider Configuration',
        'label' => 'type',
        'label_alt' => 'uid',
        'label_alt_force' => true,
        'hideAtCopy' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'origUid' => 't3_origuid',
        'editlock' => 'editlock',
        'type' => 'type',
        'typeicon_column' => 'type',
        'typeicon_classes' => [
            '0' => 'mimetypes-x-content-header',
            \Hn\Oauth2Manager\Domain\Model\InstagramProviderConfiguration::class => \Hn\Oauth2Manager\Domain\Model\InstagramProviderConfiguration::class,
            \Hn\Oauth2Manager\Domain\Model\BitbucketProviderConfiguration::class => \Hn\Oauth2Manager\Domain\Model\BitbucketProviderConfiguration::class,
            \Hn\Oauth2Manager\Domain\Model\LinkedInProviderConfiguration::class => \Hn\Oauth2Manager\Domain\Model\LinkedInProviderConfiguration::class
        ]
    ],
    'interface' => [
        'showRecordFieldList' => 'cruser_id,pid,client_id,client_secret'
    ],
    'columns' => [
        'cruser_id' => [
            'label' => 'cruser_id',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'type' => [
            'label' => 'Type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', '0'],
                    ['Instagram Provider Configuration', \Hn\Oauth2Manager\Domain\Model\InstagramProviderConfiguration::class],
                    ['Bitbucket Provider Configuration', \Hn\Oauth2Manager\Domain\Model\BitbucketProviderConfiguration::class],
                    ['LinkedIn Provider Configuration', \Hn\Oauth2Manager\Domain\Model\LinkedInProviderConfiguration::class],
                ],
            ],
        ],
        'client_id' => [
            'exclude' => 0,
            'label' => 'Client Id',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'required',
            ]
        ],
        'client_secret' => [
            'exclude' => 0,
            'label' => 'Client Secret',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'required',
            ]
        ],
        'access_tokens' => [
            'exclude' => 0,
            'label' => 'Token',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_oauth2manager_domain_model_accesstoken',
                'foreign_field' => 'provider_configuration',
                'appearance' => [
                    'collapseAll' => true,
                    'expandSingle' => true,
                    'enabledControls' => [
                        'delete' => false,
                        'info' => false,
                        'new' => false
                    ]
                ]
            ]
        ],

    ],

    'types' => [
        '0' => [
            'showitem' => 'type'
        ],
        \Hn\Oauth2Manager\Domain\Model\InstagramProviderConfiguration::class => [
            'showitem' => 'type, client_id, client_secret, access_tokens'
        ],
        \Hn\Oauth2Manager\Domain\Model\BitbucketProviderConfiguration::class => [
            'showitem' => 'type, client_id, client_secret, access_tokens'
        ],
        \Hn\Oauth2Manager\Domain\Model\LinkedInProviderConfiguration::class => [
            'showitem' => 'type, client_id, client_secret, access_tokens'
        ]
    ]
];