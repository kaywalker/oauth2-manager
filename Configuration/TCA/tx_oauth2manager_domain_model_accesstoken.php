<?php

return [
    'ctrl' => [
        'title' => 'Access Token',
        'label' => 'access_token',
        'hideAtCopy' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'origUid' => 't3_origuid',
        'editlock' => 'editlock',
        'hideTable' => true
    ],
    'interface' => [
        'showRecordFieldList' => 'cruser_id,pid,provider_configuration,access_token,expires,refresh_token,resource_owner_id'
    ],
    'columns' => [
        'cruser_id' => [
            'label' => 'cruser_id',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'provider_configuration' => [
            'label' => 'Provider Configuration',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_oauth2manager_domain_model_providerconfiguration',
                'readOnly' => true
            ],
        ],
        'access_token' => [
            'exclude' => 0,
            'label' => 'Access token',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'readOnly' => true
            ]
        ],
        'expires' => [
            'exclude' => 0,
            'label' => 'Expires',
            'config' => [
                'type' => 'input',
                'eval' => 'datetime',
                'readOnly' => true
            ]
        ],
        'refresh_token' => [
            'exclude' => 0,
            'label' => 'Refresh token',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'readOnly' => true
            ]
        ],
        'resource_owner_id' => [
            'exclude' => 0,
            'label' => 'Resource owner id',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'readOnly' => true
            ]
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => 'type,provider_configuration,access_token,expires,refresh_token,resource_owner_id'
        ]
    ]
];