<?php

namespace Hn\Oauth2Manager\Controller;

use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;

class AdministrationController extends ActionController
{
    /**
     * @var string
     */
    protected $defaultViewObjectName = BackendTemplateView::class;
    /**
     * @var \Hn\Oauth2Manager\Domain\Repository\ProviderConfigurationRepository
     * @inject
     */
    protected $providerConfigurationRepository;

    /**
     * @param ViewInterface $view
     */
    protected function initializeView(ViewInterface $view)
    {
        /** @var BackendTemplateView $view */
        parent::initializeView($view);

        if ($view instanceof BackendTemplateView) {
            $view->getModuleTemplate()->getPageRenderer()->loadRequireJsModule($this->getContextMenuModuleName());
        }
    }

    /**
     *
     */
    public function indexAction()
    {
        $this->view->assign('configurations', $this->providerConfigurationRepository->findAll());
    }

    /**
     * @return string
     */
    private function getContextMenuModuleName()
    {
        if (VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version) >= 8007000) {
            return 'TYPO3/CMS/Backend/ContextMenu';
        } else {
            return 'TYPO3/CMS/Backend/ClickMenu';
        }
    }
}
