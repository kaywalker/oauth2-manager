<?php

namespace Hn\Oauth2Manager\Controller;

use Hn\Oauth2Manager\Domain\Model\AccessToken;
use Hn\Oauth2Manager\Domain\Model\ProviderConfiguration;
use Hn\Oauth2Manager\Domain\Repository\AccessTokenRepository;
use Hn\Oauth2Manager\Exception\Oauth2StateMatchException;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\ResourceOwnerAccessTokenInterface;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class AuthorizationController extends ActionController
{
    /**
     * @var \Hn\Oauth2Manager\Services\Oauth2Service
     * @inject
     */
    protected $oauth2Service;
    /**
     * @var \Hn\Oauth2Manager\Domain\Repository\AccessTokenRepository
     * @inject
     */
    protected $accessTokenRepository;

    /**
     * @param ProviderConfiguration $providerConfiguration
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function authorizeAction(ProviderConfiguration $providerConfiguration)
    {
        $provider = $providerConfiguration->getProvider();

        $authorizationUrl = $provider->getAuthorizationUrl();

        $this->oauth2Service->storeState($provider->getState());

        $this->redirectToUri($authorizationUrl);
    }

    /**
     * @param ProviderConfiguration $providerConfiguration
     * @param string $code
     * @param string $state
     * @throws IdentityProviderException
     * @throws Oauth2StateMatchException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function callbackAction(ProviderConfiguration $providerConfiguration, $code, $state = null)
    {
        if (!$state) {
            throw new Oauth2StateMatchException('state GET parameter is missing.');
        }

        $sessionState = $this->oauth2Service->getState();

        if (!$sessionState) {
            throw new Oauth2StateMatchException('state SESSION parameter is missing.');
        }
        if ($sessionState !== $state) {
            throw new Oauth2StateMatchException('state GET and SESSION parameter do not match.');
        }

        $this->oauth2Service->storeState(null);

        $accessToken = $providerConfiguration->getProvider()->getAccessToken('authorization_code', [
            'code' => $code
        ]);

        $accessToken = $this->accessTokenRepository->createOrUpdate(
            $providerConfiguration,
            $accessToken
        );

        $this->view->assign('providerConfiguration', $providerConfiguration);
        $this->view->assign('accessToken', $accessToken);
    }
}