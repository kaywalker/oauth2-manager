<?php

namespace Hn\Oauth2Manager\Services;

use Hn\Oauth2Manager\Domain\Model\AccessToken;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;

class ApiService
{
    /**
     * @param AccessToken $accessToken
     * @param $resourceUri
     * @return mixed
     * @throws IdentityProviderException
     */
    public function get(AccessToken $accessToken, $resourceUri)
    {
        $provider = $accessToken->getProvider();

        $request = $provider->getAuthenticatedRequest(
            AbstractProvider::METHOD_GET,
            $resourceUri,
            $accessToken->getOauth2AccessToken()
        );

        return $provider->getParsedResponse($request);
    }
}