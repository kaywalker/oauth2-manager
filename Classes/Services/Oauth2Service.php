<?php

namespace Hn\Oauth2Manager\Services;

use Hn\Oauth2Manager\Domain\Model\ProviderConfiguration;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Psr\Http\Message\RequestInterface;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;

class Oauth2Service
{
    /**
     * session key used to store the oauth2 state
     */
    const SESSION_KEY_OAUTH2_STATE = 'oauth2state';

    public function storeState($state)
    {
        $this->getBackendUser()->setAndSaveSessionData(self::SESSION_KEY_OAUTH2_STATE, $state);
    }

    public function getState()
    {
        return $this->getBackendUser()->getSessionData(self::SESSION_KEY_OAUTH2_STATE);
    }

    /**
     * @return BackendUserAuthentication
     */
    private function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }
}