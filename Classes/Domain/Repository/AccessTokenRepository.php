<?php

namespace Hn\Oauth2Manager\Domain\Repository;

use Hn\Oauth2Manager\Domain\Model\AccessToken;
use Hn\Oauth2Manager\Domain\Model\ProviderConfiguration;
use League\OAuth2\Client\Token\AccessTokenInterface;
use League\OAuth2\Client\Token\ResourceOwnerAccessTokenInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class AccessTokenRepository
 * @package Hn\Oauth2Manager\Domain\Repository
 *
 * @method AccessToken findOneByProviderConfiguration(ProviderConfiguration $providerConfiguration)
 */
class AccessTokenRepository extends Repository
{
    /**
     * @param ProviderConfiguration $providerConfiguration
     * @param AccessTokenInterface $accessToken
     * @return bool true if a new acces token was created false if it was updated
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function createOrUpdate(ProviderConfiguration $providerConfiguration, AccessTokenInterface $accessToken)
    {
        $existingAccessToken = $this->getExistingToken($providerConfiguration, $accessToken);

        if ($existingAccessToken) {

            $existingAccessToken->update($accessToken);

            $this->update($existingAccessToken);

            return $existingAccessToken;
        } else {

            $newAccessToken = new AccessToken();
            $newAccessToken->setProviderConfiguration($providerConfiguration);
            $newAccessToken->update($accessToken);

            $this->add($newAccessToken);

            return $newAccessToken;
        }
    }

    /**
     * @param ProviderConfiguration $providerConfiguration
     * @param AccessTokenInterface $accessToken
     * @return AccessToken|null
     */
    private function getExistingToken(ProviderConfiguration $providerConfiguration, AccessTokenInterface $accessToken)
    {
        if ($accessToken instanceof ResourceOwnerAccessTokenInterface) {
            return $this->findOneByProviderConfigurationAndResourceOwnerId($providerConfiguration, $accessToken->getResourceOwnerId());
        }

        return $this->findOneByProviderConfiguration($providerConfiguration);
    }

    /**
     * @param ProviderConfiguration $providerConfiguration
     * @param string $resourceOwnerId
     * @return object|AccessToken|null
     */
    private function findOneByProviderConfigurationAndResourceOwnerId(ProviderConfiguration $providerConfiguration, string $resourceOwnerId = null)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd([
                $query->equals('providerConfiguration', $providerConfiguration),
                $query->equals('resourceOwnerId', $resourceOwnerId)
            ])
        );

        return $query->execute()->getFirst();
    }

    /**
     * @param $type
     * @return array|AccessToken[]
     */
    public function findByProviderConfigurationType($type)
    {
        $query = $this->createQuery();

        $query->getQuerySettings()->setRespectStoragePage(false);

        $query->matching(
            $query->equals('providerConfiguration.type', $type)
        );

        return $query->execute()->toArray();
    }
}