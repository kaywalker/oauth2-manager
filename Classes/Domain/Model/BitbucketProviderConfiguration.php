<?php

namespace Hn\Oauth2Manager\Domain\Model;

use Stevenmaguire\OAuth2\Client\Provider\Bitbucket;

class BitbucketProviderConfiguration extends ProviderConfiguration
{
    /**
     * @var string
     */
    protected $clientId;
    /**
     * @var string
     */
    protected $clientSecret;

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @return \League\OAuth2\Client\Provider\AbstractProvider|Bitbucket
     */
    public function getProvider()
    {
        return new Bitbucket([
            'clientId' => $this->getClientId(),
            'clientSecret' => $this->getClientSecret(),
            'redirectUri' => $this->getRedirectUri()
        ]);
    }
}