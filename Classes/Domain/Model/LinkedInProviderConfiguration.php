<?php

namespace Hn\Oauth2Manager\Domain\Model;

use League\OAuth2\Client\Provider\GenericProvider;

class LinkedInProviderConfiguration extends ProviderConfiguration
{
    /**
     * @var string
     */
    protected $clientId;
    /**
     * @var string
     */
    protected $clientSecret;

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @return \League\OAuth2\Client\Provider\AbstractProvider|GenericProvider
     */
    public function getProvider()
    {
        return new GenericProvider([
            'clientId' => $this->getClientId(),
            'clientSecret' => $this->getClientSecret(),
            'redirectUri' => $this->getRedirectUri(),
            'urlAuthorize' => 'https://www.linkedin.com/oauth/v2/authorization',
            'urlAccessToken' => 'https://www.linkedin.com/oauth/v2/accessToken',
            'urlResourceOwnerDetails' => 'https://api.linkedin.com/v2/me'
        ]);
    }
}