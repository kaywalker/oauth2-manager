<?php

namespace Hn\Oauth2Manager\Domain\Model;

use Hn\Oauth2Manager\Provider\InstagramProvider;

class InstagramProviderConfiguration extends ProviderConfiguration
{
    /**
     * @var string
     */
    protected $clientId;
    /**
     * @var string
     */
    protected $clientSecret;

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @return InstagramProvider|\League\OAuth2\Client\Provider\AbstractProvider
     */
    public function getProvider()
    {
        return new InstagramProvider([
            'clientId' => $this->getClientId(),
            'clientSecret' => $this->getClientSecret(),
            'redirectUri' => $this->getRedirectUri()
        ]);
    }
}