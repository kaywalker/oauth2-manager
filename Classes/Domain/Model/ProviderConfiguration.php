<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 22.11.18
 * Time: 13:06
 */

namespace Hn\Oauth2Manager\Domain\Model;

use League\OAuth2\Client\Provider\AbstractProvider;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

abstract class ProviderConfiguration extends AbstractEntity
{
    /**
     * @var string
     */
    protected $type;
    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hn\Oauth2Manager\Domain\Model\AccessToken>
     */
    protected $accessTokens;

    public function __construct()
    {
        $this->accessTokens = new ObjectStorage();
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return AccessToken[]
     */
    public function getAccessTokens(): array
    {
        return $this->accessTokens->toArray();
    }

    /**
     * @return AbstractProvider
     */
    abstract public function getProvider();

    /**
     * @return string
     */
    public function getRedirectUri()
    {
        $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['oauth2_manager']);
        $callbackPath = $extensionConfiguration['authorizationCallbackPath'];

        return sprintf('%s/%s/%s/', GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST'), trim($callbackPath, '/'), $this->getUid());
    }
}