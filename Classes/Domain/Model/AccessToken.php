<?php

namespace Hn\Oauth2Manager\Domain\Model;

use League\OAuth2\Client\Token\AccessTokenInterface;
use League\OAuth2\Client\Token\ResourceOwnerAccessTokenInterface;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class AccessToken extends AbstractEntity
{
    /**
     * @var \Hn\Oauth2Manager\Domain\Model\ProviderConfiguration
     */
    protected $providerConfiguration;
    /**
     * @var string
     */
    protected $accessToken;
    /**
     * @var int
     */
    protected $expires;
    /**
     * @var string
     */
    protected $refreshToken;
    /**
     * @var string
     */
    protected $resourceOwnerId;

    public function update(AccessTokenInterface $accessToken)
    {
        $this->setAccessToken($accessToken->getToken());
        $this->setExpires($accessToken->getExpires());
        $this->setRefreshToken($accessToken->getRefreshToken());

        if ($accessToken instanceof ResourceOwnerAccessTokenInterface) {
            $this->setResourceOwnerId($accessToken->getResourceOwnerId());
        }
    }
    /**
     * @return \League\OAuth2\Client\Provider\AbstractProvider
     */
    public function getProvider()
    {
        return $this->providerConfiguration->getProvider();
    }

    /**
     * @return ProviderConfiguration
     */
    public function getProviderConfiguration(): ProviderConfiguration
    {
        return $this->providerConfiguration;
    }



    /**
     * @param ProviderConfiguration $providerConfiguration
     */
    public function setProviderConfiguration(ProviderConfiguration $providerConfiguration): void
    {
        $this->providerConfiguration = $providerConfiguration;
    }


    /**
     * @return \League\OAuth2\Client\Token\AccessToken|null
     */
    public function getOauth2AccessToken()
    {
        if (!$this->accessToken) {
            return null;
        }

        return new \League\OAuth2\Client\Token\AccessToken([
            'access_token' => $this->accessToken,
            'expires' => $this->expires,
            'refresh_token' => $this->refreshToken,
            'resource_owner_id' => $this->resourceOwnerId
        ]);
    }


    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return int
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @param int $expires
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return string
     */
    public function getResourceOwnerId()
    {
        return $this->resourceOwnerId;
    }

    /**
     * @param string $resourceOwnerId
     */
    public function setResourceOwnerId($resourceOwnerId)
    {
        $this->resourceOwnerId = $resourceOwnerId;
    }
}