<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 04.12.18
 * Time: 17:56
 */

namespace Hn\Oauth2Manager\Provider;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class InstagramResourceOwner implements ResourceOwnerInterface
{

    /**
     * Raw response
     *
     * @var array
     */
    protected $response;

    /**
     * InstagramResourceOwner constructor.
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->response = $response;
    }

    /**
     * Returns the identifier of the authorized resource owner.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->response['data']['id'];
    }

    /**
     * Return all of the owner details available as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->response;
    }
}