<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 23.11.18
 * Time: 10:24
 */

namespace Hn\Oauth2Manager\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Core\Bootstrap;
use TYPO3\CMS\Extbase\Mvc\Web\FrontendRequestHandler;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class AuthorizationCallbackHook
{
    /**
     * @param $params
     * @throws \TYPO3\CMS\Core\Error\Http\ServiceUnavailableException
     */
    public function execute(&$params, TypoScriptFrontendController $typoScriptFrontendController)
    {

        $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['oauth2_manager']);

        $callbackPath = $extensionConfiguration['authorizationCallbackPath'];

        list($prefix, $providerConfigurationUid) = explode('/', trim(strtok($typoScriptFrontendController->siteScript, '?'), '/'));

        if ($prefix !== trim($callbackPath, '/') || !$providerConfigurationUid) {
            return;
        }

        $typoScriptFrontendController->determineId();
        $typoScriptFrontendController->initTemplate();
        $typoScriptFrontendController->getConfigArray();

        $typoScriptService = $this->getTypoScriptService();
        $pluginConfiguration = $typoScriptService->convertTypoScriptArrayToPlainArray($typoScriptFrontendController->tmpl->setup['plugin.']['tx_oauth2manager.']);

        $_GET['tx_oauth2manager_oauth2manager'] = [
            'providerConfiguration' => $providerConfigurationUid,
            'code' => $_GET['code'],
            'state' => $_GET['state']
        ];

        unset($_GET['code']);
        unset($_GET['state']);

        // disable chash validation for this execution
        $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFoundOnCHashError'] = false;

        $bootstrap = GeneralUtility::makeInstance(Bootstrap::class);
        echo $bootstrap->run('', [
            'vendorName' => 'Hn',
            'extensionName' => 'Oauth2Manager',
            'pluginName' => 'Oauth2Manager',
            'switchableControllerActions' => [
                'Authorization' => ['callback']
            ],
            'mvc' => [
                'requestHandlers' => [FrontendRequestHandler::class => FrontendRequestHandler::class],
            ],
            'persistence' => $pluginConfiguration['persistence'],
            'view' => $pluginConfiguration['view'],
            'settings' => $pluginConfiguration['settings'],
        ]);

        exit();
    }

    /**
     * @return object|\TYPO3\CMS\Extbase\Service\TypoScriptService|\TYPO3\CMS\Core\TypoScript\TypoScriptService
     */
    private function getTypoScriptService()
    {
        if (VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version) >= 8007000) {
            return GeneralUtility::makeInstance(\TYPO3\CMS\Core\TypoScript\TypoScriptService::class);
        } else {
            return GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Service\TypoScriptService::class);
        }
    }
}