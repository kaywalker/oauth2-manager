CREATE TABLE tx_oauth2manager_domain_model_providerconfiguration (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,
	editlock tinyint(4) DEFAULT '0' NOT NULL,

  type varchar(255) NOT NULL DEFAULT '0',
  access_tokens int(11) NOT NULL DEFAULT '0',

	client_id varchar(255) DEFAULT '' NOT NULL,
	client_secret varchar(255) DEFAULT '' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
);

CREATE TABLE tx_oauth2manager_domain_model_accesstoken (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,
	editlock tinyint(4) DEFAULT '0' NOT NULL,

  provider_configuration int(11) DEFAULT NULL,

	access_token varchar(512) DEFAULT '' NOT NULL,
	expires int(11) DEFAULT NULL,
	refresh_token varchar(512) DEFAULT NULL,
	resource_owner_id varchar(255) DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
);