<?php
defined('TYPO3_MODE') or die();


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Hn.Oauth2Manager',
    'Oauth2Manager',
    [
        'Authorization' => 'callback',
    ],
    [
        'Authorization' => 'callback',
    ]
);


if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['checkAlternativeIdMethods-PostProc'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['checkAlternativeIdMethods-PostProc'] = [];
}

array_unshift($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['checkAlternativeIdMethods-PostProc'], \Hn\Oauth2Manager\Hooks\AuthorizationCallbackHook::class .'->execute');

if (TYPO3_MODE === 'BE') {
    $icons = [
        \Hn\Oauth2Manager\Domain\Model\InstagramProviderConfiguration::class => 'Instagram.svg',
        \Hn\Oauth2Manager\Domain\Model\BitbucketProviderConfiguration::class => 'Bitbucket.svg',
        \Hn\Oauth2Manager\Domain\Model\LinkedInProviderConfiguration::class => 'LinkedIn.svg',
    ];
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    foreach ($icons as $identifier => $path) {
        $iconRegistry->registerIcon(
            $identifier,
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:oauth2_manager/Resources/Public/Icons/' . $path]
        );
    }
}
