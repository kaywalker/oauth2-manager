<?php

if (TYPO3_MODE === 'BE') {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'Hn.Oauth2Manager',
        'tools',
        'Oauth2 Manager',
        '',
        [
            'Administration' => 'index',
            'Authorization' => 'authorize',
        ],
        [
            'access'    => 'user,group',
            'icon'      => 'EXT:oauth2_manager/Resources/Public/Icons/module.svg',
            'labels' => 'LLL:EXT:oauth2_manager/Resources/Private/Language/locallang_module.xlf'
        ]
    );

}